# docker-compose for a local pihole server

## Steps:

1. Download this directory to the location you want to have PiHole run out of.
	- cd to install directory (e.g. `mkdir -p ~/.pihole && cd ~/.pihole`)
	- `curl -L https://gitlab.com/avionix/pihole-docker/-/archive/master/pihole-docker-master.tar.gz | tar -zxv --strip-components 1 && ./makedirs.sh`
2. Run `./makedirs.sh`
3. Ensure that your current DNS resolver is stopped (and disabled):
	- `systemctl stop systemd-resolved && systemctl disable systemd-resolved`
	- This assumes you're using systemd-resolved.
3. `docker-compose up -d` to start the container.
4. Add `127.0.0.1` as the first entry in `/etc/resolv.conf`

