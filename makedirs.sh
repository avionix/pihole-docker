#!/usr/bin/env sh

mkdir -v -p etc-dnsmasq.d etc-pihole
echo ""
echo "docker-compose up -d    to start the container."
echo "docker logs pihole      to see the container logs."
